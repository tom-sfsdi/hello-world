package com.toml.hello;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloWorldController.class)
public class HelloWorldControllerTest {

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    void setUp() {

    }

    @Test
    void sayHello_noParam_ReturnsHelloWorld() throws Exception {

        // Set up
        mockMvc.perform(get("/hello"))

                // Assertion
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));

    }

    @Test
    void sayHelloMyName_ReturnsHelloName() throws Exception {
        mockMvc.perform(get("/hello?name=Tom"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Tom"));
    }
}



